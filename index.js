//Ejercicio review dom 

/*1.1 Basandote en el array siguiente, crea una lista ul > li 
dinámicamente en el html que imprima cada uno de los paises.
const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela']; 

const countries = ["Japón", "Nicaragua", "Suiza", "Australia", "Venezuela"];

let countryList = document.createElement("ul");

for (const country of countries) {
  let countryItems = document.createElement("li");
  countryItems.textContent = country;
  countryList.appendChild(countryItems);
}
document.body.appendChild(countryList);
console.log(countries);
*/

/*1.2 Elimina el elemento que tenga la clase .fn-remove-me. 
let deleteElement = document.querySelector(".fn-remove-me");
deleteElement.remove();
*/

/*1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
en el div de html con el atributo data-function="printHere".
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

const cars = ["Mazda 6", "Ford fiesta", "Audi A4", "Toyota corola"];
let findDiv = document.querySelector('[data-function="printHere"]');
let createList = document.createElement("ul");
for (const car of cars) {
  let createCar = document.createElement("li");
  createCar.textContent = car;
  createList.appendChild(createCar);
}
findDiv.appendChild(createList);
 */

/*1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
h4 para el titulo y otro elemento img para la imagen.
const countries = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
]; 

const countries = [
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=1" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=2" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=3" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=4" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=5" },
];
for (const country of countries) {
    let createCountry = document.createElement("div");
    createCountry.innerHTML = `<h4>${country.title}</h4><img src=${country.imgUrl} />`;
    document.body.appendChild(createCountry);
}
*/

/* 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
elemento de la lista. 

const deleteLastButton = document.querySelector("#delete-last-element");
deleteLastButton.addEventListener("click", function () {
  const selectDiv = document.querySelectorAll("div");
  selectDiv[selectDiv.length - 1].remove();
});
*/

/* 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
elementos de las listas que elimine ese mismo elemento del html. */

const countries = [
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=1" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=2" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=3" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=4" },
    { title: "Random title", imgUrl: "https://picsum.photos/300/200?random=5" },
  ];
  
  for (const country of countries) {
    let createCountry = document.createElement("div");
    createCountry.innerHTML = `<h4>${country.title}</h4><img src=${country.imgUrl} />`;
  
    let deleteButton = document.createElement("button");
    deleteButton.textContent = "Remove";
  
    createCountry.appendChild(deleteButton);
  
    deleteButton.addEventListener("click", function () {
        createCountry.remove();
    });
  
    document.body.appendChild(createCountry);
  }